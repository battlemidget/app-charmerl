requires 'App::Cmd::Setup';
requires 'Data::Printer';
requires 'File::Copy::Recursive';
requires 'File::ShareDir';
requires 'IPC::Run3';
requires 'Moo';
requires 'Moo::Role';
requires 'Path::Tiny';
requires 'Sub::Exporter';
requires 'perl', '5.016';

on configure => sub {
    requires 'Module::Build::Tiny', '0.024';
};

on test => sub {
    requires 'Test::More';
};
