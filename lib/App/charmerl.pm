package App::charmerl;

use strictures 1;
use 5.014;
our $VERSION = '0.003';
use App::Cmd::Setup -app;

1;
__END__

=encoding utf-8

=head1 NAME

App::charmerl - juju charms meets perl

=head1 SYNOPSIS

    #!/usr/bin/env charmerl

    # Installer ruleset

    charm:attributes {
	    # Configuration attributes
	    set:var ROOT_PATH < "/srv/$UNIT_NAME"
	    get:var PAYLOAD < "app_payload"
	    get:var GROUP_CODE_RUNNER < "group_code_runner"
	    get:var GROUP_CODE_OWNER < "group_code_owner"
    }

    charm:ruleset {
	    # Simple info logger
	    log:info "Performing installation"

	    # Path namespace utilizing builtin mkpath
	    path:mkpath "/srv/$UNIT_NAME"

	    # File namespace to extract a file
	    file:extract [ src => "$PAYLOAD",
			   dst => "$ROOT_PATH" ]

	    log:info "Adding additional apt repositories"

	    # Apt support batch addition of PPAS
	    apt:add-repo [ "ppa:gunicorn/ppa",
			   "ppa:juju/charms" ]

	    # Update apt cache
	    apt:update

	    # Install specific packages using Q( ) array
	    apt:install Q( python-dev python-django python-requests )

	    # Python PIP namespace support and optional requirements file
	    pip:install -r "$UNIT_DIR/requirements.txt"
	    pip:install Q( gunicorn )

	    # Permissions
	    perm:addgroup Q( $GROUP_CODE_RUNNER $GROUP_CODE_OWNER )
    }

=head1 DESCRIPTION

App::charmerl is a parser for sugar coating some of the common tasks.

=head1 AUTHOR

Adam Stokes E<lt>adam.stokes@ubuntu.comE<gt>

=head1 COPYRIGHT

Copyright 2013- Adam Stokes

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 DISCLAIMER

This is merely a personal project to satisfy my needs for using something
other than bash/python for writing charms. Do not use this in production.

=head1 SEE ALSO

=cut
