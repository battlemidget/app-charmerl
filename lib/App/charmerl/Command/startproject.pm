package App::charmerl::Command::startproject;
use App::charmerl -command;
use strict;
use warnings;
use Moo;
use Path::Tiny;
use Data::Printer;

with('App::charmerl::Role::StartProject');

sub validate_args {
    my ($self, $opt, $args) = @_;
    $self->usage_error("a NAME is required.") unless $args->[0];
}

sub abstract {
    return "Start a new charmerl project";
}

sub description {
    return "Creates a charm project skeleton.";
}

sub usage_desc {
    return "%c startproject [NAME]";
}

sub execute {
    my ($self, $opts, $args) = @_;
    $self->projectname(path($args->[0]));
    $self->new_project;
}

1;
