package App::charmerl::DSL;
use strict;
use warnings;
use 5.014;

use Data::Printer;
use Path::Tiny;
use Moo;

with ('App::charmerl::Role::DSL');

1;
