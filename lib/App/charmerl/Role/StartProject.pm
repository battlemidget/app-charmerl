use strictures 1;
package App::charmerl::Role::StartProject;

use Path::Tiny;
use File::Copy::Recursive qw(dircopy);
use File::ShareDir ':ALL';
use Moo::Role;

has 'projectname' => (is => 'rw');
has 'skeleton' => (
    is      => 'rw',
    default => sub {
        my $self = shift;
        path(dist_dir('App-charmerl'), 'charmerldir');
    },
);

sub new_project {
    my ($self) = @_;

    dircopy($self->skeleton, $self->projectname);
}

1;
