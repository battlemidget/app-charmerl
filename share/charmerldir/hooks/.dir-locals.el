((nil . ((indent-tabs-mode . nil)
		 (tab-width . 2)
		 (tab-stop-list (number-sequence 2 200 2))
		 (fill-column . 80))))
